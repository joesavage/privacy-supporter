#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

require "sinatra"
require 'koala'
require 'htmlentities'
require 'securerandom'

#ActiveRecord & DB connection
require 'sinatra/activerecord'
require './environments'
set :raise_errors, false
set :show_exceptions, false
set :static, true
set :static_cache_control, [:public, max_age: 60 * 60 * 24 * 365] #Caching max_age of a year (specified in seconds)

unless ENV["COOKIE_SECRET"]
	abort("missing env vars: please set COOKIE_SECRET with your app credentials")
end
use Rack::Session::EncryptedCookie, :secret => ENV["COOKIE_SECRET"]

use Rack::Deflater
use Rack::Static, :urls => ['/stylesheets', '/images'], :root => 'public'

FACEBOOK_SCOPE = 'publish_actions,user_likes,email'

unless ENV["FACEBOOK_APP_ID"] && ENV["FACEBOOK_SECRET"]
	abort("missing env vars: please set FACEBOOK_APP_ID and FACEBOOK_SECRET with your app credentials")
end

require_relative 'helpers/update'

before do
	# HTTPS redirect
	if settings.environment == :production && request.scheme != 'https'
	redirect "https://#{request.env['HTTP_HOST']}"
	end

	response.headers["X-Frame-Options"] = "ALLOW-FROM https://apps.facebook.com"

	update_users
end

helpers do
	require_relative 'helpers/init'
end

require_relative 'models/init'
require_relative 'routes/init'