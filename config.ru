require "rubygems"
require "bundler"

Bundler.require

use Rack::ShowExceptions

require "./app"
run Sinatra::Application
