get "/close" do # used to close the browser window opened to post to wall/send to friends
	"<body onload='window.close();'/>"
end
get "/auth/facebook" do # Allows for direct oauth authentication
	session[:access_token] = nil
	redirect authenticator.url_for_oauth_code(:permissions => FACEBOOK_SCOPE)
end
get '/auth/facebook/callback' do
	session[:access_token] = authenticator.get_access_token(params[:code])
	redirect '/'
end
get "/preview/logged_out" do # Doesn't actually sign out permanently, but good for testing
	session[:access_token] = nil
	request.cookies.keys.each { |key, value| response.set_cookie(key, '') }
	redirect '/'
end


require_relative '../helpers/test_environment.rb'

get '/vidalia' do
	halt(404) unless ENV["RACK_ENV"] == "development"
	erb :vidalia, :layout => false
end
post '/vidalia' do
	halt(404) unless ENV["RACK_ENV"] == "development"

	if params[:nickname] and params[:contact] and params[:port]
		params[:nickname] = HTMLEntities.new.encode(params[:nickname])

		node = random_nodes(1).first

		token = params[:contact].scan(/<fb-token:(.+)>/).first
		if not token.nil?
			token = token.first
			match = User.all.select { |user| user.facebook_token == token }.first

			if not match.nil?
				new_node = Node.new
				new_node['fingerprint'] = node['fingerprint']
				new_node['user_facebook_id'] = match['facebook_id']
				new_node['last_seen'] = node['last_seen']
				new_node['first_seen'] = node['first_seen']
				new_node['running'] = node['running']
				new_node['consensus_weight'] = node['consensus_weight']

				new_node['observed_bandwidth'] = node['observed_bandwidth']
				new_node['exit_node'] = node['exit_node']
				new_node['platform'] = node['platform']
				new_node['country'] = node['country']
				new_node['nickname'] = params[:nickname]

				new_node.save
				update_users(true)
			end
		end
	end

	redirect '/vidalia?updated=true', 303
end