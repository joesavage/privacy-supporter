#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

get "/" do
	get_records
	if @user_record.nil? or @user_record['newbie'] == 0
		if params[:get_started]
			@user_record['newbie'] = 1
			@user_record.save
			redirect '/'
		else
			update_stats #TODO?: Don't call on page requests -- call periodically
			@page = "index"
			erb :index
		end
	else
		if @node_records.nil? or @node_records.count < 1 #They know the cause but have no nodes.
			if params[:newbie]
				@user_record['newbie'] = 0
				@user_record.save
				redirect '/'
			else
				@page = "first-setup"
				erb :step_by_step
			end
		elsif @user_record.is_newbie? #They have nodes and know the cause - time for a promotion!
			@user_record['newbie'] = 2
			@user_record.save
			@page = "first-setup"
			erb :congratulations #Woo, they set up their very first Tor node! They grow up so fast!
		else #They have nodes and aren't new.
			@dashboard = true
			@page = "dashboard"
			erb :user #Dashboard
		end
	end
end

# used by Canvas apps - redirect the POST to be a regular GET
post "/" do
	redirect "/"
end
