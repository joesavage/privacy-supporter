#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

get '/achievements' do
	get_records
	erb :achievements
end