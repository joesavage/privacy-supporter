#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

get '/nodes/:fingerprint' do
	get_records(false)
	@node = Node.where(:fingerprint => params[:fingerprint]).limit(1).first || halt(404)
	@owner = User.where(:facebook_id => @node['user_facebook_id']).limit(1).first
	erb :node
end

get '/new_node' do
	get_records
	erb :step_by_step
end