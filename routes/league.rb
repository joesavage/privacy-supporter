#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

get '/user_league' do
	get_records(false)
	@ranked_users = User.order("score DESC") #Do we have to fetch _all_ the records?!
	@page = "user_league"

	@entries_per_page = 10
	@pages = (@ranked_users.size / @entries_per_page.to_f).ceil
	params[:page] = params[:page].to_i
	@page_number = (params[:page] != 0 and params[:page] <= @pages and params[:page] > 0) ? params[:page] : 1


	erb :user_league
end

get '/team_league' do
	get_records
	@ranked_teams = Team.all #Do we have to fetch _all_ the records?! (In this case, I think we do)
	@ranked_teams = @ranked_teams.sort_by {|team| team.score }.reverse
	@page = "team_league"

	@entries_per_page = 15
	@pages = (@ranked_teams.size / @entries_per_page.to_f).ceil
	params[:page] = params[:page].to_i
	@page_number = (params[:page] != 0 and params[:page] <= @pages and params[:page] > 0) ? params[:page] : 1

	erb :team_league
end