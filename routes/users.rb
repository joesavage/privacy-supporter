#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

get "/users/:facebook_id" do
	get_records
	@user_record = User.where(:facebook_id => params[:facebook_id]).limit(1).first || halt(404)

	if @user['id'] == @user_record['facebook_id']
		redirect '/'
	else
		@user = @graph.get_object("/#{@user_record['facebook_id']}")
		@node_records = @user_record.nodes
		@team_record = @user_record.team
		@dashboard = false
		erb :user
	end
end