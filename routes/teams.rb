#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

get "/new_team" do
	get_records(false)
	@problem = params[:problem].to_i
	erb :new_team
end

post "/new_team" do
	get_records
	name = params[:name]
	url = params[:url]

	if not url =~ /^[A-Za-z0-9\.\/:\-_]*$/ #Should be XSS safe
		redirect '/new_team?problem=1'
	elsif @user_record['team_id'] != nil or Team.exists?(:founder_facebook_id => @user_record['facebook_id'])
		redirect '/new_team?problem=2'
	elsif name.blank?
		redirect '/new_team?problem=3'
	elsif Team.exists?(:name => name)
		redirect '/new_team?problem=4'
	elsif name.length > 40 or url.length > 100
		redirect '/new_team?problem=5'
	elsif not url == "" and not url.start_with?("http://")
		redirect '/new_team?problem=6'
	else 
		new_team = Team.create(:name => HTMLEntities.new.encode(name), :url => HTMLEntities.new.encode(url), :founder_facebook_id => @user_record['facebook_id'])
		@user_record['team_id'] = new_team['id']
        @user_record.save
		redirect "/teams/#{new_team['id']}"
	end
end

get '/leave_team' do
	get_records
	halt(404) unless params[:state] == session[:state] #CSRF protection -- i.e. <img src="https://localhost:5000/leave_team"/>
	if not @user_record['team_id'].nil?
		if @team_record['founder_facebook_id'] == @user_record['facebook_id']
			User.where(:team_id => @team_record['id']).map! do |user|
				user['team_id'] = nil
				user.save
			end
			@team_record.destroy
			"Team destroyed."
		else
			@user_record['team_id'] = nil
			@user_record.save
		end
	end
	redirect '/'
end

get "/teams/:id" do
	get_records
	@team = Team.where(:id => params[:id]).limit(1).first || halt(404)

	if params[:join_team] == "true"
		halt(404) unless params[:state] == session[:state] #CSRF, as with leave_team
		user = User.where(:facebook_id => @user['id']).limit(1).first
		user['team_id'] = @team['id'] unless Team.exists?(:founder_facebook_id => user['facebook_id'])
		user.save
		redirect "/teams/#{@team['id']}"
	else
		@founder = User.where(:facebook_id => @team[:founder_facebook_id]).limit(1).first
		@members = User.where(:team_id => @team['id'])
		erb :team
	end
end