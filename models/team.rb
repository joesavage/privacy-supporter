#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

class Team < ActiveRecord::Base
	has_many :users, :class_name => "User"

	def url
		"/teams/#{self['id']}"
	end
	def score
		score = 0
		User.where(:team_id => self['id']).each do |user|
			score += user['score']
		end
		score
	end
end