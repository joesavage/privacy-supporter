#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

class User < ActiveRecord::Base
	self.primary_key = 'facebook_id'

	belongs_to :team, :foreign_key => 'team_id', :class_name => "Team"
	has_many :nodes, :class_name => "Node", :dependent => :destroy

	def url
		"/users/#{self['facebook_id']}"
	end
	def nodes
		Node.where(:user_facebook_id => self['facebook_id'])
	end
	def team
		Team.where(:id => self['team_id']).limit(1).first
	end
	def facebook_token
		Digest::SHA1.hexdigest(self['facebook_id'] + self['salt'])[0..7] #16^8 values should be strong enough
	end
	def is_newbie?
		self['newbie'] < 2
	end


	def achievements
		achievements = [] #Achievements are non-persistent, so we don't have to store them
		achievements << NodeTypeAchievement.new("Relay Owner", "Set up a Tor relay node", self, false)
		achievements << NodeTypeAchievement.new("Exit Owner", "Set up a Tor exit node", self, true)
		achievements << NodeNumberAchievement.new("Magic Number", "Set up three Tor nodes", self, 3)
		achievements << NodePlatformAchievement.new("Early Adopter", "Own a Tor node on an alpha software build", self, /\-alpha/)
		achievements << NodeFirstSeenAchievement.new("Veteran", "Own a node for over a year", self, 365)
		achievements << UserScoreAchievement.new("Influence", "Have a score of 800 league points or over", self, 800)
		achievements << UserTeamScoreAchievement.new("Teamwork", "Be part of a team that has 10,000 league points or over", self, 10000)
		achievements << UserRefererAchievement.new("Two Friends", "Refer two friends to this app", self, 2)
		achievements << UserRefererAchievement.new("Recruiter", "Refer ten friends to this app", self, 10)

		achievements
	end
end