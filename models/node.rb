#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

class Node < ActiveRecord::Base
	self.primary_key = 'fingerprint'
	
	belongs_to :user, :foreign_key => 'user_facebook_id', :class_name => "User"

	def url
		"/nodes/#{self['fingerprint']}"
	end
end