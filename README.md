# README

File structure:

- **/db** -- Database stuff
	- **/migrate** -- Migrations
	- **schema.rb** -- The database schema
- **/helpers** -- Helper methods belong here
	- **init.rb** -- This file is what gets included into the application. It should require any files in the '/helpers' directory that you need.
- **/models** -- Model files belong here
	- **init.rb** -- This file is what gets included into the application. It should require any models in the '/models' directory that you need.
- **/public** -- Static files belong here
	- **/images** -- Images belong here
	- **/stylesheets** -- Stylesheets belong here
		- **style.scss** -- The application's main SCSS stylesheet
		- **style.css** -- The application's main stylesheet, compiled from SCSS using SASS with `--style compressed`
- **/routes** -- All Sinatra HTTP routes belong here
	- **init.rb** -- This file is what gets included into the application. It should require any routes in the '/routes' directory that you need.
- **/tmp** -- Temporary storage belongs here
- **/views** -- View files belong here
	- **layout.erb** -- The default layout
- **.env** -- A file with this name should exist to set the `FACEBOOK_APP_ID`, `FACEBOOK_SECRET`, `RACK_ENV`, and `COOKIE_SECRET` environment variables
- **Gemfile** -- The Gemfile
- **LICENSE** -- The license file which dictates usage and redistribution of the code
- **Procfile** -- A standard Procfile with a process for starting the application
- **Rakefile** -- A standard Rakefile, primarily for the `rake db:migrate` and `rake db:rollback` database commands, which also contains a `test_environment` task to generate an environment with a few users, nodes, and teams.
- **app.rb** -- The heart of the application where everything is `require`d and brought together
- **config.ru** -- A standard config.ru file which kickstarts the application
- **environments.rb** -- Database connections are established here
- **test_environment.json** -- A JSON file with (currently un-updated) node information for tasks which require test data - update and modify this as you see fit