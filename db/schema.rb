# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130717104049) do

  create_table "nodes", id: false, force: true do |t|
    t.string   "fingerprint",        limit: 40,                     null: false
    t.string   "user_facebook_id",   limit: 20,                     null: false
    t.string   "nickname",                      default: "Unnamed", null: false
    t.datetime "last_seen",                                         null: false
    t.datetime "first_seen",                                        null: false
    t.boolean  "running",                                           null: false
    t.integer  "consensus_weight",                                  null: false
    t.integer  "observed_bandwidth"
    t.boolean  "exit_node",                     default: false,     null: false
    t.string   "platform"
    t.string   "country"
  end

  add_index "nodes", ["fingerprint"], name: "index_nodes_on_fingerprint", unique: true, using: :btree

  create_table "teams", force: true do |t|
    t.string "name",                           null: false
    t.string "url"
    t.string "founder_facebook_id", limit: 20, null: false
  end

  add_index "teams", ["name"], name: "index_teams_on_name", unique: true, using: :btree

  create_table "users", id: false, force: true do |t|
    t.string  "facebook_id", limit: 20,             null: false
    t.string  "referer_id",  limit: 20
    t.string  "salt",                               null: false
    t.integer "team_id"
    t.integer "newbie",      limit: 2,  default: 0
    t.integer "score",                  default: 0, null: false
    t.string  "email"
  end

  add_index "users", ["facebook_id"], name: "index_users_on_facebook_id", unique: true, using: :btree

end
