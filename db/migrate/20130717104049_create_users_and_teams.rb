#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

class CreateUsersAndTeams < ActiveRecord::Migration
	def up
		create_table :nodes, :id => false do |t|
			t.string :fingerprint, :limit => 40, :null => false, :options => 'PRIMARY KEY'
			t.string :user_facebook_id, :limit => 20, :null => false
			t.string :nickname, :default => 'Unnamed', :null => false

			#Onionoo-derived fields
			t.datetime :last_seen, :null => false
			t.datetime :first_seen, :null => false
			t.boolean :running, :null => false
			t.integer :consensus_weight, :null => false
			t.integer :observed_bandwidth
			t.boolean :exit_node, :default => false, :null => false
			t.string :platform
			t.string :country
		end
		add_index :nodes, :fingerprint, :unique => true

		create_table :users, :id => false do |t|
			t.string :facebook_id, :limit => 20, :null => false, :options => 'PRIMARY KEY'
			t.string :referer_id, :limit => 20
			t.string :salt, :null => false
			t.integer :team_id, :default => nil
			t.integer :newbie, :limit => 1, :default => 0 #Newbie: 0->Doesn't know cause, 1->No nodes yet, 2->Knows cause and has nodes.
			t.integer :score, :null => false, :default => 0
			t.string :email
		end
		add_index :users, :facebook_id, :unique => true

		create_table :teams do |t|
			t.string :name, :limit => 255, :null => false
			t.string :url, :limit => 255
			t.string :founder_facebook_id, :limit => 20, :null => false
		end
		add_index :teams, :name, :unique => true

		#FK Constrains
		execute "ALTER TABLE users ADD CONSTRAINT fk_user_referer_id FOREIGN KEY (referer_id) REFERENCES users(facebook_id)"
		execute "ALTER TABLE nodes ADD CONSTRAINT fk_node_user_facebook_id FOREIGN KEY (user_facebook_id) REFERENCES users(facebook_id)"
		execute "ALTER TABLE teams ADD CONSTRAINT fk_team_founder_id FOREIGN KEY (founder_facebook_id) REFERENCES users(facebook_id)"
		execute "ALTER TABLE users ADD CONSTRAINT fk_user_team_id FOREIGN KEY (team_id) REFERENCES teams(id)"
	end

	def down
		execute "DROP TABLE nodes CASCADE" #drop_table doesn't provide an option to CASCADE (which is dumb)
		execute "DROP TABLE users CASCADE"
		execute "DROP TABLE teams CASCADE"
	end
end