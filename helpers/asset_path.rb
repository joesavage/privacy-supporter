#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

def asset_path(asset)
	ext = File.extname(asset)
	asset.chomp!(ext)
	final_ext = ext

	dir = case ext
	      when '.js' then '/javascripts'
	      when '.css' then '/stylesheets'
	      when '.scss'
	      	final_ext = '.css'
	      	'/stylesheets'
	      when '.png', '.jpg', '.gif' then '/images'
	      else
	      	raise "Extension, '#{ext}', is unknown"
	      end

	qstring = File.mtime(File.join('public', dir, "#{asset}#{ext}")).to_i.to_s
	File.join(dir, "#{asset}#{final_ext}?#{qstring}")
end