#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

require 'net/https'
require 'json'
require 'time'
require 'htmlentities'
require 'set'

def update_json(stats)
	Dir.mkdir("tmp") unless File.exists?("tmp")

	if stats
		source = "https://onionoo.torproject.org/details?fields=exit_policy_summary,observed_bandwidth,country_name"
		destination = "tmp/onionoo_for_stats.json"
	else
		source = "https://onionoo.torproject.org/details?contact=<fb-token:"
		destination = "tmp/onionoo_for_users.json" 
	end

	stats_condition =     (stats and (not defined?(@@stats_fetch) or Time.now - @@stats_fetch > (60 * 60 * 2) or not File.exists?(destination)))
	users_condition = (not stats and (not defined?(@@users_fetch) or Time.now - @@users_fetch > (60 * 15) or not File.exists?(destination)))

	if stats_condition or users_condition
		uri = URI.parse(URI.escape(source))
		http = Net::HTTP.new(uri.host, uri.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE #VERIFY_PEER to require verification of SSL certificate used

		req = Net::HTTP::Get.new(uri.request_uri)
		req['If-Modified-Since'] = File.stat(destination).mtime.rfc2822 if File.exists?(destination)
		req['Accept-Encoding'] = "gzip"

		response = http.start do |http|
			http.request(req)
		end

		stats ? @@stats_fetch = Time.now : @@users_fetch = Time.now

		if not response.is_a? Net::HTTPNotModified
			File.open(destination, 'w') { |file| file.write(response.body) }
			true #Return 'true' to indicate that the data has changed
		else
			false
		end
	else
		false
	end
end

def last_updated(stats)
	Time.rfc2822(stats ? (File.stat("tmp/onionoo_for_stats.json").mtime.rfc2822 if File.exists?("tmp/onionoo_for_stats.json")) : (File.stat("tmp/onionoo_for_users.json").mtime.rfc2822 if File.exists?("tmp/onionoo_for_users.json")))
end
def last_fetched(stats)
	stats ? @@stats_fetch : @@users_fetch
end

def update_stats
	if update_json(true) or not (defined?(@@bridges) and defined?(@@relays) and defined?(@@exits) and defined?(@@bandwidth) and defined?(@@countries))
		response = JSON.parse(File.read('tmp/onionoo_for_stats.json'))

		@@bridges = response['bridges'].size
		@@relays = 0
		@@exits = 0
		@@bandwidth = 0
		@@countries = Set.new
		response['relays'].map! do |relay|
			if exit_node?(relay['exit_policy_summary'])
				@@exits += 1
			else
				@@relays += 1
			end
			@@bandwidth += relay['observed_bandwidth'].to_i if not relay['observed_bandwidth'].nil?
			@@countries.add(HTMLEntities.new.encode(relay['country_name'])) if not relay['country_name'].nil? 
		end
		@@countries = @@countries.size
	end
end
def update_users(force=false)
	if update_json(false) or force
	  # Search for new nodes and add to database, update DB stats for existing nodes, and update user scores.
	  response = JSON.parse(File.read('tmp/onionoo_for_users.json'))
		response['relays'].map! do |relay|
			if not relay['contact'].nil? and relay['contact'] =~ /<fb-token:.+>/
				token = relay['contact'].scan(/<fb-token:(.+)>/).first.first
				match = User.all.select { |user| user.facebook_token == token }.first #There should only be one result if there are no token collisions

				if match.to_s != ""
					if not Node.exists?(:fingerprint => relay['fingerprint'])
						node = Node.new
					else
						node = Node.where(:fingerprint => relay['fingerprint']).limit(1).first
					end

					node['fingerprint'] = relay['fingerprint']
					node['user_facebook_id'] = match['facebook_id']
					node['last_seen'] = DateTime.strptime(relay['last_seen'], "%Y-%m-%d %H:%M:%S") #UTC timestamp (YYYY-MM-DD hh:mm:ss)
					node['first_seen'] = DateTime.strptime(relay['first_seen'], "%Y-%m-%d %H:%M:%S")
					node['running'] = relay['running']
					node['consensus_weight'] = relay['consensus_weight'].to_i

					node['observed_bandwidth'] = relay['observed_bandwidth'].to_i if not relay['observed_bandwidth'].nil?
					node['exit_node'] = exit_node?(relay['exit_policy_summary'])
					node['platform'] = HTMLEntities.new.encode(relay['platform']) if not relay['platform'].nil?
					node['country'] = HTMLEntities.new.encode(relay['country_name']) if not relay['country_name'].nil?
					node['nickname'] = HTMLEntities.new.encode(relay['nickname']) if not relay['nickname'].nil?

					node.save
				end
			end
		end

		User.all.map! do |user|
			user['score'] = 0
			Node.where(:user_facebook_id => user['facebook_id']).each do |node|
				if age_in_days(node['last_seen']) < 7 and node['running'] == true
					user['score'] += node['consensus_weight']
				end
			end

			user.save
		end
	end
end