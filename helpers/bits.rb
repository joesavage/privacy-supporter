#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

def age_in_days(time_stamp, now = Time.now)
	(now - time_stamp) / (60 * 60 * 24)
end
def host
	request.env['HTTP_HOST']
end
def scheme
	request.scheme
end
def url_no_scheme(path = '')
	"//#{host}#{path}"
end
def url(path = '')
	"#{scheme}://#{host}#{path}"
end
def hashzip(a, b) #[a, b, c], [d, e, f] => [a => d, b => e, c => f]
	Hash[a.zip(b)]
end
def percentage(a, b)
	((a.to_f/b.to_f)*100).to_i
end
def gender_pronoun(gender)
	gender == "male" ? "his" : "her"
end