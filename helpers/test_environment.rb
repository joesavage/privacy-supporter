#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

def random_nodes(number)
	nodes = []
	response = JSON.parse(File.read('test_environment.json'))
	response['relays'].select{|relay| relay['running']}.shuffle.first(number).map do |relay|
		node = {}
		node['fingerprint'] = relay['fingerprint']
		node['last_seen'] = DateTime.strptime(relay['last_seen'], "%Y-%m-%d %H:%M:%S") #UTC timestamp (YYYY-MM-DD hh:mm:ss)
		node['first_seen'] = DateTime.strptime(relay['first_seen'], "%Y-%m-%d %H:%M:%S")
		node['running'] = relay['running']
		node['consensus_weight'] = relay['consensus_weight'].to_i
		node['observed_bandwidth'] = relay['observed_bandwidth'].to_i if not relay['observed_bandwidth'].nil?
		node['exit_node'] = exit_node?(relay['exit_policy_summary'])
		node['platform'] = HTMLEntities.new.encode(relay['platform']) if not relay['platform'].nil?
		node['country'] = HTMLEntities.new.encode(relay['country_name']) if not relay['country_name'].nil?
		node['nickname'] = HTMLEntities.new.encode(relay['nickname']) if not relay['nickname'].nil?
		nodes << node
	end

	nodes
end