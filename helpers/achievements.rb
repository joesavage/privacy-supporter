#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

module Achievement
	def initialize
		raise 'This method may not be called.'
	end
	def is_earned?
		raise 'This method must be overridden.'
	end
	def name
		@name
	end
	def description
		@description
	end
	def progress
		nil
	end
end

class NodeNumberAchievement
	include Achievement
	def initialize(name, description, user, threshold)
		@name = name
		@description = description
		@user = user
		@threshold = threshold
	end
	def is_earned?
		Node.where(:user_facebook_id => @user['facebook_id']).size >= @threshold
	end
	def progress
		percentage(Node.where(:user_facebook_id => @user['facebook_id']).size, @threshold)
	end
end

class NodeTypeAchievement
	include Achievement
	def initialize(name, description, user, exit_node)
		@name = name
		@description = description
		@user = user
		@exit_node = exit_node
	end
	def is_earned?
		Node.where(:user_facebook_id => @user['facebook_id']).any? { |node| node['exit_node'] == @exit_node }
	end
end

class NodePlatformAchievement
	include Achievement
	def initialize(name, description, user, match)
		@name = name
		@description = description
		@user = user
		@match = match
	end
	def is_earned?
		Node.where(:user_facebook_id => @user['facebook_id']).any? { |node| node['platform'] =~ @match }
	end
end

class NodeFirstSeenAchievement
	include Achievement
	def initialize(name, description, user, days)
		@name = name
		@description = description
		@user = user
		@days = days
	end
	def is_earned?
		Node.where(:user_facebook_id => @user['facebook_id']).any? { |node| age_in_days(node['first_seen']) >= @days }
	end
	def progress
		nodes = Node.where(:user_facebook_id => @user['facebook_id'])
		nodes.size > 0 ? percentage(age_in_days(nodes.max_by { |node| age_in_days(node['first_seen']) }['first_seen']), @days) : 0
	end
end

class UserRefererAchievement
	include Achievement
	def initialize(name, description, user, referers)
		@name = name
		@description = description
		@user = user
		@referers = referers
	end
	def is_earned?
		User.where(:referer_id => @user['facebook_id']).size >= @referers
	end
	def progress
		percentage(User.where(:referer_id => @user['facebook_id']).size, @referers)
	end
end

class UserScoreAchievement
	include Achievement
	def initialize(name, description, user, threshold)
		@name = name
		@description = description
		@user = user
		@threshold = threshold
	end
	def is_earned?
		@user['score'] >= @threshold
	end
	def progress
		percentage(@user['score'], @threshold)
	end
end

class UserTeamScoreAchievement
	include Achievement
	def initialize(name, description, user, threshold)
		@name = name
		@description = description
		@user = user
		@threshold = threshold
	end
	def is_earned?
		@user.team.nil? ? false : @user.team.score >= @threshold
	end
	def progress
		@user.team.nil? ? 0 : percentage(@user.team.score, @threshold)
	end
end