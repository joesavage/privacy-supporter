#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

def exit_node?(policy_summary)
	if policy_summary.nil?
		false
	elsif not policy_summary['reject'].nil?
		policy_summary['reject'][0] != "1-65535"
	else
		true
	end
end