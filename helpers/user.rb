#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

def get_user(fbid)
	User.where(:facebook_id => fbid).limit(1).first
end
def average_observed_bandwidth(nodes)
	avg = 0
	if nodes.count != 0
		nodes.each do |node|
			avg += node['observed_bandwidth']
		end
		avg /= nodes.count
	end
	nice_bytes(avg)
end