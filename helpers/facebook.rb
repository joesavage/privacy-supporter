#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

def authenticator
	@authenticator ||= Koala::Facebook::OAuth.new(ENV["FACEBOOK_APP_ID"], ENV["FACEBOOK_SECRET"], url("/auth/facebook/callback"))
end
def access_token_from_cookie #Allow for JavaScript authentication
	session[:state] = SecureRandom.hex #When we're picking up a new access token, refresh the session 'state' (CSRF protection)
	authenticator.get_user_info_from_cookies(request.cookies)['access_token']
rescue => err
	warn err.message
end
def access_token
	session[:access_token] || access_token_from_cookie
end
def get_records(user=true)
	@graph = Koala::Facebook::API.new(access_token) #Get base API Connection
	@@app = @graph.get_object(ENV["FACEBOOK_APP_ID"]) if not defined?(@@app) or @@app.nil? #[SLOW] Get public details of current application

	if user
		halt(404) unless access_token
		@user = @graph.get_object("me") #[REALLY SLOW] Get the current user's Facebook object
		facebook_id = @user['id'].to_s

		if User.exists?(:facebook_id => facebook_id)
			@user_record  = User.where(:facebook_id => facebook_id).limit(1).first
		else
			@user_record = User.new
			@user_record['facebook_id'] = facebook_id
			@user_record['salt'] = SecureRandom.hex
			@user_record['email'] = @user['email']
			@user_record['referer_id'] = params[:referer].to_i unless params[:referer].nil? or not User.exists?(:facebook_id => params[:referer].to_i)
			@user_record.save
		end

		@team_record  = Team.where(:id => @user_record['team_id']).limit(1).first
		@node_records = Node.where(:user_facebook_id => @user_record['facebook_id'])
	end
end
def get_fb_user(user)
	if @user and user['facebook_id'] == @user['id']
		@user
	else
		@graph.get_object("#{user['facebook_id']}")
	end
end
def get_fb_user_from_id(id)
	@graph.get_object("#{id}")
end
def get_fb_users(users)
	@graph.batch do |batch_api|
		users.each do |user|
			batch_api.get_object(user['facebook_id'])
		end
	end
end

def get_fb_image(user, type='square')
	@graph.get_picture("#{user['facebook_id']}", :type => type)
end
def get_fb_image_from_fb_user(user, type='square')
	@graph.get_picture(user['id'], :type => type)
end
def get_fb_images(users, type='square')
	@graph.batch do |batch_api|
		users.each do |user|
			batch_api.get_picture(user['facebook_id'], :type => type)
		end
	end
end
def get_random_friend_for_referal
	#TODO: Performing a DB query for every friend and then only using one seems pretty inefficient. This can probably be improved.
	@graph.get_connections('me', 'friends').select { |user| not User.exists?(:facebook_id => user['id']) }.shuffle.first
end