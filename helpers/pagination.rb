#Copyright 2013, Joe Savage
#This software is distributed under the BSD 2-Clause License. See the file LICENSE for further details.

def pagination(page_number, pages)
	if pages == 1
		""
	elsif page_number == pages
		"<a href=\"?page=#{page_number - 1}\">&lt;&lt; Previous</a>"
	elsif page_number == 1
		"<a href=\"?page=#{page_number + 1}\">Next &gt;&gt;</a>"
	else
		"<a href=\"?page=#{page_number - 1}\">&lt;&lt; Previous</a> | <a href=\"?page=#{page_number + 1}\">Next &gt;&gt;</a>"
	end
end